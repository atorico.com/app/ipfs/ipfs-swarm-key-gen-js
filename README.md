# node-ipfs-swarm-key-gen
This program generates swarm.key file for IPFS Private Network feature.

# Installation
```sh
npm install git+https://gitlab.com/atorico.com/app/ipfs/ipfs-swarm-key-gen-js.git -g
```
# Usage
```sh
node-ipfs-swarm-key-gen > ~/.ipfs/swarm.key
```
Change ~/.ipfs/ to different directory if you use custom IPFS_PATH.

# License
MIT
